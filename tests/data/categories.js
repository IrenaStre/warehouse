const {ObjectID} = require('mongodb');

module.exports = [{
	_id: new ObjectID(),
	name: 'First test category'
}, {
	_id: new ObjectID(),
	name: 'Second test category'
}];