const {ObjectID} = require('mongodb');

module.exports = [{
	_id: new ObjectID(),
	name: 'First test product',
	describe: 'First test product describe'
}, {
	_id: new ObjectID(),
	name: 'Second test product',
	describe: 'Second test product decribe'
}];