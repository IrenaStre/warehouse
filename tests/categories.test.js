const expect = require('expect');
const request = require('supertest');
const app = require('../src/app.js');
const {Category} = require('../src/models/category');
const categories = require('./data/categories');

describe('CRUD', () => {
	let categoryId = categories[1]._id;
	describe('CREATE', () => {
		it('should create category', (done) => {
			request(app)
				.post('/categories')
				.send({action: 'create', _id: categoryId, name: categories[1].name})
				.expect(200)
				.expect((res) => {
					console.log(res.body);
					res.body._id = categoryId;
					res.body.name =categories[1].name;
				})
				.end((err, res) => {
					if (err) {
						return done(err);
					}
					return done();
				});
		});
	});

	describe('GET', () => {
		it('should get list of categories', (done) => {
			request(app)
				.get('/categories')
				.expect(200)
				.expect((res) => { 
					console.log(res.body);
					res.body.isArray;
				})
				.end(done);
		});

		it('should get category by ID', (done) => {
			request(app)
				.get(`/categories?categoryId=${categoryId}`)
				.expect(200)
				.expect((res) => { 
					res.body._id = categoryId;
				})
				.end(done);
		});

		it('should return 404 if category not found', (done) => {
			request(app)
				.get('/productsc?ategoryId=2')
				.expect(404)
				.expect((res) => {
					res.body;
				})
				.end(done);
		});
	});

    describe('UPDATE', () => {
    	it('should update category', (done) => {
    		request(app)
				.put('/categories')
				.send({action: 'update', id: categoryId, name: categories[0].name})
				.expect(200)
				.expect((res) => {
					console.log(res.body);
					res.body._id = categoryId;
					res.body.name = categories[0].name
				})
				.end((err, res) => {
					if (err) {
						return done(err);
					}
					return done();
				});
    	});
    });

	describe('DELETE', () => {
		it('should delete category', (done) => {
			request(app)
				.delete(`/categories`)
				.send({id: categoryId})
				.expect(200)
				.expect((res) => {
				console.log(res.body);
					res.body._id = categoryId;
				})
				.end(done);
		});

		it('should return 404 if category not found', (done) => {
			request(app)
				.get(`/categories?categoryId=${categoryId}`)
				.expect(404)
				.expect((res) => {
//console.log(res.text);
				})
				.end(done);
		});

		it('should return 404 if ObjectID is not valid', (done) => {
			request(app)
				.get('/categories?categoryId=5')
				.expect((res) => {
console.log(res.text);
					res.text = 'Category ID invalid';
				})
				.end(done);
		});
	});
});
