const expect = require('expect');
const request = require('supertest');
const app = require('../src/app.js');
const {Product} = require('../src/models/product');
const categories = require('./data/categories');
const products = require('./data/products');

describe('CRUD', () => { 
	let categoryId = categories[1]._id;
	let productId = products[1]._id; 
	describe('CREATE', () => {
		it('should create product', (done) => {
			request(app)
				.post(`/products`)
				.send({action: 'create', _id: productId, categoryId: categoryId, name: products[1].name, describe: products[1].describe})
				.expect(200)
				.expect((res) => {
					console.log(res.body);
					res.body._id = productId;
					res.body.name = products[1].name;
				})
				.end((err, res) => {
					if (err) {
						return done(err);
					}
					return done();
				});
		});
	});

	describe('GET', () => {
		it('should get list of products', (done) => {
			request(app)
				.get(`/products?categoryId=${categoryId}`)
				.expect(200)
				.expect((res) => { 
					console.log(res.body);
					res.body.isArray;
				})
				.end(done);
		});

		it('should get product by ID', (done) => {
			request(app)
				.get(`/products?productId=${productId}`)
				.expect(200)
				.expect((res) => { 
					res.body._id = productId;
				})
				.end(done);
		});

		it('should return 404 if category not found', (done) => {
			request(app)
				.get('/products?categoryId=2')
				.expect(404)
				.expect((res) => {
					res.body;
				})
				.end(done);
		});
});

    describe('UPDATE', () => {
    	it('should update product', (done) => {
    		request(app)
				.put('/products')
				.send({action: 'update', id: productId, name: products[0].name, describe: products[0].describe})
				.expect(200)
				.expect((res) => {
					console.log(res.body);
					res.body._id = productId;
					res.body.name = products[0].name
				})
				.end((err, res) => {
					if (err) {
						return done(err);
					}
					return done();
				});
    	});
    });

	describe('DELETE', () => {
		it('should delete product', (done) => {
			request(app)
				.delete(`/products`)
				.send({id: productId})
				.expect(200)
				.expect((res) => {
				console.log(res.body);
					res.body._id = productId;
				})
				.end(done);
		});


		it('should return 404 if product not found', (done) => {
			request(app)
				.get(`/products?productId=${productId}`)
				.expect(404)
				.expect((res) => {
console.log(res.text);
				})
				.end(done);
		});

		it('should return 404 if ObjectID is not valid', (done) => {
			request(app)
				.get('/products?productId=5')
				.expect((res) => {
console.log(res.text);
					res.text = 'Product ID invalid';
				})
				.end(done);
		});
	});
});
