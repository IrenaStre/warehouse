const express = require('express');
const routes = express.Router();
const get = require('../controllers/categories/getCategories');
const create = require('../controllers/categories/createCategories');
const update = require('../controllers/categories/updateCategories');
const del = require('../controllers/categories/deleteCategories');

routes.route('/')
	.get(get)
	.post(create)
	.put(update)
	.delete(del);

module.exports = routes;