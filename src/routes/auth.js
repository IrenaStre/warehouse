const express = require('express');
const routes = express.Router();
const login = require('../controllers/auth/auth');

routes.post('/login', login.localAuth);

module.exports = routes;