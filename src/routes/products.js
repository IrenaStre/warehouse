const express = require('express');
const routes = express.Router();
const get = require('../controllers/products/getProducts');
const create = require('../controllers/products/createProducts');
const update = require('../controllers/products/updateProducts');
const del = require('../controllers/products/deleteProducts');


routes.route('/')
	.get(get)
	.post(create)
	.put(update)
	.delete(del);

module.exports = routes;	