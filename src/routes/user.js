const express = require('express');
const routes = express.Router();
const get = require('../controllers/users/getUser');
const create = require('../controllers/users/createUsers');
const update = require('../controllers/users/updateUsers');


routes.route('/')
    .get(get)
	.post(create)
	.put(update);

module.exports = routes;