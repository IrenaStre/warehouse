exports.port = process.env.PORT || 3000;
exports.host = process.env.HOST;

exports.db = {
	url: 'mongodb://localhost:27017/Warehouse', 
	options: {useNewUrlParser: true}
};

exports.jwtSecret = "MyS3cr3tK3Y";

exports.jwtSession = {
    session: false
};

