const localStrategy = require('passport-local').Strategy;
const bcrypt = require('bcryptjs');
const User = require('../models/user');

module.exports = new localStrategy({
        usernameField: 'username',
        passwordField: 'password'
    }, 
    async (username, password, done) => {
        try{  
            const user = await User.findOne({username});
            if (!user) {              
                return done(null, false, {message: 'Incorrect username.'});
            }
            const match = await bcrypt.compare(password, user.password);
            if (!match) { 
                return done(null, false, {message: 'Incorrect password.'});
            }

            return done(null, user, {message: 'Logged In Successfully'});
      
      }catch(err) {
         done(err);
      } 
    }
);