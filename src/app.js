const port = require('./config');
const express = require('express');
const passport = require('passport');
const localStrategy = require('./config/localStrategy');
const jwtStrategy = require('./config/jwtStrategy');
const userRoutes = require('./routes/user');
const authRoutes = require('./routes/auth');
const categoryRoutes = require('./routes/categories');
const productRoutes = require('./routes/products');
const bodyParser = require('body-parser');
const app = express();

app.use(bodyParser.json());
app.use('/', authRoutes);
app.use('/user', userRoutes);
app.use('/categories', categoryRoutes);
app.use('/products', productRoutes);
passport.use('local', localStrategy);
passport.use('jwt', jwtStrategy);


app.listen(port, () => {
	console.log('Server started');
});

module.exports = app;