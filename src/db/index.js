const {mongoose} = require('./dbConnect');

exports.all = (object, params) => {
	return object.find(params);
}

exports.getById = (object, id) => {
	return object.findById(id);
}

exports.create = (object, params) => { 
	return object.create(params);
}

exports.update = (object, id, params) => {
	return object.findByIdAndUpdate(id, params, {new: true});
}

exports.delete = (object, id) => {
	return object.findByIdAndRemove(id);
}                 

exports.deletes = (object, params) => { 
	return object.deleteMany(params);
}