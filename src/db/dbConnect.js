
const mongoose = require('mongoose');
const db = require('../config');

mongoose.Promise = global.Promise;

mongoose.connect(db.db.url, db.db.options);

module.exports = mongoose; 
