const mongoose = require('mongoose');

const productSchema = new mongoose.Schema({
	name: {
		type: String,
		required: true,
		minlength: 1
	},
	describe: String,
	categoryId: Object
});

const Product = mongoose.model('product', productSchema);

module.exports = Product;