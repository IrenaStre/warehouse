const mongoose = require('mongoose');
const validator = require('validator');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

const userSchema = new mongoose.Schema ({
	username: {
	    type: String,
	    required: true
	},
  	email: {
        type: String,
        required: true,
        trim: true,
        minlength: 1,
        unique: true,
        validate: {
            validator: validator.isEmail,
            message: '{VALUE} is not a valid email'
        }
    },
    password: {
	    type: String,
	    required: true,
        trim: true,
        minlength: 6
    }
});

userSchema.pre('save', function (next) {
    var user = this;

    if (user.isModified('password')) {
        bcrypt.hash(user.password, (err, hash) => {
            user.password = hash;
            next();
        });
    } else {
        next();
    }
});

const User = mongoose.model('User', userSchema);

module.exports = User;