const db = require('../../db');
const {ObjectID} = require('mongodb');
const User = require('../../models/user');
const getToken = require('../auth/auth');

exports.getById = (req, res) => {
	let id;

	if ((req.query) && (req.query.userId)) {
	
		if (ObjectID.isValid(req.query.userId)) {
			db.getById(User, req.query.userId)
				.then((user) => {
		    		if (!user) throw new Error (`User not found`);
					return user;
					//res.send(user);
				})
				.catch((err) => {
					console.log(`Get error ${err}`);
					res.status(404).send(`${err}`);
				});
		} else {
			return res.status(404).send(`${User} ID invalid`);
		}	
	} 

	


}	