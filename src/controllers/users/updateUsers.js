const db = require('../../db');
const {ObjectID} = require('mongodb');
const User = require('../../models/user');

module.exports = (req, res) => {  
    if (ObjectID.isValid(req.body.id)) {
		db.update(User, req.body.id, {
						 username: req.body.username, 
		 				 email: req.body.email, 
		 				 password: req.body.password})
			.then((users) => {
				res.send(users);
			})
			.catch((err) => {
				console.log(`Get error ${err}`);
				res.status(404).send(err);
			});	
	} else {
		return res.status(404).send('User ID invalid');
	}
}
