const db = require('../../db');
const {ObjectID} = require('mongodb');
const User = require('../../models/user');

module.exports = (req, res) => {
	let id;
    let user = new User({
		_id: (req.body._id) ? req.body._id : id,
		username: req.body.username, 
		email: req.body.email, 
		password: req.body.password
	});

    user.save()
    	.then(() => {
			return user.generateAuthToken();
    	})
		.then((token) => {	
			res.send(user);
		})
		.catch((err) => {
			console.log(`Get error ${err}`);
			res.status(404).send(err);
		});	
}
	
