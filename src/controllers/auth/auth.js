const db = require('../../db');
const {ObjectID} = require('mongodb');
const passport = require('passport');
const jwt = require('jsonwebtoken');
const secret = require('../../config');
const User = require('../../models/user');

exports.localAuth =  (req, res, next) => {
    passport.authenticate('local', {session: false}, (err, user, info) => {
        if (err || !user) {
            return res.status(400).json({
                message: 'Something is not right',
                user   : user
            });
        }
       req.login({ user: user.toObject() }, {session: false}, (err) => {
           if (err) {
               res.send(err);
           }
           const token = jwt.sign({ user: user._id }, secret.jwtSecret);
           return res.json({user, token});
        });
    })(req, res);
};

exports.jwtAuth = (req, res, next) => {
	passport.authenticate('jwt', {session: false}),
		(req, res) => {
			res.send(req.user);
	}
};