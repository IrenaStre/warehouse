const db = require('../../db');
const {ObjectID} = require('mongodb');
const Product = require('../../models/product');

module.exports = (req, res) => { 
	if (ObjectID.isValid(req.body.id)) {
		db.delete(Product, req.body.id)
			.then((products) => {	
				res.send(products);
			})
			.catch((err) => {
				console.log(`Get error ${err}`);
				res.status(404).send(err);
			});
	} else {
		return res.status(404).send('Product ID invalid');
	}
}
