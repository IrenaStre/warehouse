const db = require('../../db');
const {ObjectID} = require('mongodb');
const Product = require('../../models/product');

module.exports = (req, res) => {  
	let id;
	let item = new Product({
		_id: (req.body._id) ? req.body._id : id,
		name: req.body.name,
		describe: req.body.describe,
		categoryId: req.body.categoryId
	});

	if (ObjectID.isValid(req.body.categoryId)) { 
		item.save()
			.then((products) => {	
				res.send(products);
			})
			.catch((err) => {
				console.log(`Get error ${err}`);
				res.status(404).send(err);
			});
	} else {
		return res.status(404).send('Category ID invalid');
	}
}

