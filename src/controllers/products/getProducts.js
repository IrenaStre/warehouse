const db = require('../../db');
const {ObjectID} = require('mongodb');
const Product = require('../../models/product');

module.exports = (req, res) => {
	let func;	 
	if ((req.query) && (req.query.productId)) {
		let productId = req.query.productId;	
		let errorMessage = 'Product';
		if (ObjectID.isValid(productId)) {
			func = db.getById(Product, productId);
		} else {
			return res.status(404).send(`${errorMessage} ID invalid`);
		}	
	} else if ((req.query) && (req.query.categoryId)) { 
		let categoryId = req.query.categoryId;
		let errorMessage = 'Products';
		if (ObjectID.isValid(categoryId)) {			
			func = db.all(Product, {categoryId: categoryId});
		}  else {  console.log(req.query.categoryId);
			return res.status(404).send(`Category ID invalid`);
		}
	} else{
		throw new Error('WTF');
	} 	

	func.then((result) => {
			if (!result) throw new Error(`${errorMessage} not found`);
			res.send(result);
		})
		.catch((err) => {
			console.log(`Get error ${err}`);
			res.status(404).send(err);
		});
}
