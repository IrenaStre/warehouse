const db = require('../../db');
const {ObjectID} = require('mongodb');
const Product = require('../../models/product');

module.exports = (req, res) => { 
	let id = req.body.id;
	if (ObjectID.isValid(id)) {
		let params = {};
		if ((req.body) && (req.body.name)) {
			params.name = req.body.name;
		}
		if ((req.body) && (req.body.describe)) {
			params.describe = req.body.describe;
		}		
		db.update(Product, id, params)
			.then((result) => {
				res.send(result);
			})
			.catch((err) => {
				console.log(`Get error ${err}`);
				res.status(404).send(err);
			});
	} else {
		return res.status(404).send('Product ID invalid');
	}	
}

