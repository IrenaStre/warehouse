const db = require('../../db');
const {ObjectID} = require('mongodb');
const Category = require('../../models/category');

module.exports = (req, res) => {
	let id;
	db.create(Category, {_id: (req.body._id) ? req.body._id : id, name: req.body.name})
		.then((categories) => {
			res.send(categories);
		})
		.catch((err) => {
			console.log(`Get error ${err}`);
			res.status(404).send(err);
		});	
}
