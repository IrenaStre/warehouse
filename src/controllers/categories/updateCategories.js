const db = require('../../db');
const {ObjectID} = require('mongodb');
const Category = require('../../models/category');

module.exports = (req, res) => {  
    if (ObjectID.isValid(req.body.id)) {
		db.update(Category, req.body.id, {name: req.body.name})
			.then((categories) => {
				res.send(categories);
			})
			.catch((err) => {
				console.log(`Get error ${err}`);
				res.status(404).send(err);
			});	
	} else {
		return res.status(404).send('Category ID invalid');
	}
}

