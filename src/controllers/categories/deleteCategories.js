const db = require('../../db');
const {ObjectID} = require('mongodb');
const Category = require('../../models/category');
const Product = require('../../models/product');

module.exports = (req, res) => {  console.log('WTF');
	let deletedCategory;
	let deletedProducts
    if (ObjectID.isValid(req.body.id)) {
		db.delete(Category, req.body.id)
			.then((result) => {
				deletedCategory = result;
				console.log(`DELETE RESULT ${result}`);
				if(db.all(Product, {categoryId: req.body.id})) {
					deletedProducts = db.deletes(Product, {categoryId: req.body.id})
				} 
				return deletedProducts;
			})
			.then((deletedProducts) => {
				console.log(`DELETE products RESULT ${deletedProducts}`);
				res.send({
					deletedCategory: deletedCategory,
					deletedProducts: deletedProducts
				});
			})
			.catch((err) => {
				console.log(`Get error ${err}`);
				res.status(404).send(err);
			});	
	} else {
		return res.status(404).send('Category ID invalid');
	}		
}