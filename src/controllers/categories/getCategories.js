const db = require('../../db');
const {ObjectID} = require('mongodb');
const Category = require('../../models/category');

module.exports = (req, res) => { 
	let func;
	let errorMessage; 
	if ((req.query) && (req.query.categoryId)) {
		errorMessage = 'Category'; 
		if (ObjectID.isValid(req.query.categoryId)) {
			func = db.getById(Category, req.query.categoryId);
		} else {
			return res.status(404).send(`${errorMessage} ID invalid`);
		}	
	} else {
		let errorMessage = 'Categories';
		func = db.all(Category, {});
	}

	func.then((categories) => {
		    if (!categories) throw new Error (`${errorMessage} not found`);
			res.send(categories);
		})
		.catch((err) => {
			console.log(`Get error ${err}`);
			res.status(404).send(`${err}`);
		});
}
